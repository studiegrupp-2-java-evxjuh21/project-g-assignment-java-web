package com.example.projectg.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotBlank
    private String activity;

    @Column
    private int priority;

    @Column
    private boolean isCompleted;

    public Todo(String activity, int priority, boolean isCompleted) {
        this.activity = activity;
        this.priority = priority;
        this.isCompleted = isCompleted;
    }

    public Todo() {

    }

    public int getId() {
        return id;
    }

    public String getActivity() {
        return activity;
    }

    public int getPriority() {
        return priority;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }
}
