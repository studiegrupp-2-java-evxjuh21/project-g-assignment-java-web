package com.example.projectg.views;

import com.example.projectg.components.TodoForm;
import com.example.projectg.entities.Todo;
import com.example.projectg.services.TodoService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import java.util.Comparator;

@Route(value = "/", layout = AppView.class)
@PageTitle("Todo")
public class TodoView extends VerticalLayout {

    TodoService todoService;
    Grid<Todo> gridNotDone = new Grid<>(Todo.class, false);
    Grid<Todo> gridDone = new Grid<>(Todo.class, false);
    TodoForm todoForm;

    public TodoView(TodoService todoService) {
        this.todoService = todoService;
        this.todoForm = new TodoForm(todoService, this);

        VerticalLayout mainLayout = new VerticalLayout();

        H2 notDoneTitle = new H2("Att göra");
        H2 doneTitle = new H2("Slutförda");
        H2 newTodoTitle = new H2("Ny Todo");

        mainLayout.setAlignItems(Alignment.CENTER);

        mainLayout.add(notDoneTitle, configureGridNotDone(), doneTitle, configureGridDone(), newTodoTitle, todoForm);
        todoForm.setTodo(new Todo("", 1, false));
        add(mainLayout);

    }

    public Component configureGridNotDone() {
        gridNotDone.setItems(
                todoService.findAll()
                        .stream()
                        .filter(todo -> !todo.isCompleted())
                        .sorted(Comparator.comparing(Todo::getPriority))
                        .toList()
        );
        return populateGrid(gridNotDone);
    }

    public Component configureGridDone() {
        gridDone.setItems(
                todoService.findAll()
                        .stream()
                        .filter(Todo::isCompleted)
                        .sorted(Comparator.comparing(Todo::getPriority))
                        .toList()
        );
        return populateGrid(gridDone);
    }

    public Component populateGrid(Grid<Todo> grid) {

        grid.setWidthFull();
        grid.addColumn(Todo::getActivity).setHeader("Aktivitet");
        grid.addColumn(Todo::getPriority).setHeader("Prio").setSortable(true);

        // Button to edit todo
        grid.addComponentColumn(todo -> {
            Button editButton = new Button(new Icon(VaadinIcon.EDIT), event -> {
                Dialog editDialog = new Dialog();
                TodoForm dialogForm = new TodoForm(todoService, this);
                dialogForm.setTodo(todo);
                editDialog.add(new H3("Redigera Todo"), dialogForm);
                editDialog.open();
            });

            editButton.addThemeVariants(ButtonVariant.LUMO_SMALL);


            return editButton;
        }).setHeader("Redigera");

        // Button to mark todo as done/not done
        grid.addComponentColumn(todo -> {
            Button doneButton = new Button(new Icon(VaadinIcon.CHECK), event -> {
                todoService.updateCompletedById(todo.getId());
                updateView();
            });

            doneButton.addThemeVariants(ButtonVariant.LUMO_SUCCESS);
            doneButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
            doneButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

            return doneButton;
        }).setHeader("Klarmarkera");

        // Button to delete todo
        grid.addComponentColumn(todo -> {
            Button deleteButton = new Button(new Icon(VaadinIcon.CLOSE), event -> {
                todoService.deleteById(todo.getId());
                updateView();
            });

            deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
            deleteButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
            deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

            return deleteButton;
        }).setHeader("Ta bort");

        grid.setAllRowsVisible(true);
        return grid;
    }

    public void updateView() {
        gridNotDone.setItems(
                todoService.findAll()
                        .stream()
                        .filter(todo -> !todo.isCompleted())
                        .sorted(Comparator.comparing(Todo::getPriority))
                        .toList()
        );
        gridDone.setItems(
                todoService.findAll()
                        .stream()
                        .filter(Todo::isCompleted)
                        .sorted(Comparator.comparing(Todo::getPriority))
                        .toList()
        );
        todoForm.setTodo(new Todo("", 1, false));
    }

}
