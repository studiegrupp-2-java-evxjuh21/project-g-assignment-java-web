package com.example.projectg.views;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class AppView extends AppLayout {

    public AppView() {

        HorizontalLayout headerLayout = new HorizontalLayout();

        headerLayout.add(
                new H1("Studiegrupp 2")
        );
        setHeaderStyle(headerLayout);
        addToNavbar(headerLayout);

    }

    public void setHeaderStyle(HorizontalLayout headerLayout){
        headerLayout.setWidthFull();
        headerLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        headerLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        headerLayout.setMargin(true);
    }

}
