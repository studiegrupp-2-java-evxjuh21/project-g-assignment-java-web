package com.example.projectg.services;

import com.example.projectg.entities.Todo;
import com.example.projectg.repositories.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    private TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> findAll() {
        return todoRepository.findAll();
    }

    public Todo save(Todo todo) {
        return todoRepository.save(todo);
    }

    public void deleteById(int id) {
        todoRepository.deleteById(id);
    }

    public Todo updateById(int id, Todo todo) {
        Todo existingTodo = todoRepository.findById(id).orElseThrow();
        existingTodo.setActivity(todo.getActivity());
        existingTodo.setPriority(todo.getPriority());
        return todoRepository.save(existingTodo);
    }

    public void updateCompletedById(int id) {
        Todo todo = todoRepository.findById(id).orElseThrow();
        todo.setCompleted(!todo.isCompleted());
        todoRepository.save(todo);
    }
}