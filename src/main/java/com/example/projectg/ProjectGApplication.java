package com.example.projectg;

import com.example.projectg.entities.Todo;
import com.example.projectg.repositories.TodoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ProjectGApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectGApplication.class, args);
    }

    @Bean
    CommandLineRunner init(TodoRepository todoRepository) {
        return args -> {
            Todo todo1 = new Todo("Äta mat", 3, false);
            Todo todo2 = new Todo("Dricka kaffe", 2, true);
            Todo todo3 = new Todo("Spela spel?", 1, false);
            todoRepository.save(todo1);
            todoRepository.save(todo2);
            todoRepository.save(todo3);
        };
    }

}
